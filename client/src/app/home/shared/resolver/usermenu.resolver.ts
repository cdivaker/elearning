import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { apiConfig } from 'src/app/service/application-setting';
import { UserMenu } from 'src/app/model/menu';
import { AppService } from 'src/app/service/app.service';
import { AuthService } from 'src/app/service/auth-service';
import { catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UserMenuResolver implements Resolve<UserMenu> {
  /**
   *
   */
  constructor(private appService: AppService,private authService:AuthService) {
  }
  resolve(route: ActivatedRouteSnapshot): Observable<UserMenu>| Promise<UserMenu> | UserMenu {
    
    if(this.authService.isTokenExpired()){
      return this.appService.get(apiConfig.getUserMenu);
    }else{
      return of(null);
    }

    
  }
}