import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of, Subject,throwError  } from 'rxjs';
import { AppService } from 'src/app/service/app.service';
import { CategoryMaster } from '../../../model/category';
import { applicationUrlSetting } from '../../../service/application-setting';
import { apiConfig } from 'src/app/service/application-setting';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CategoryResolver implements Resolve<CategoryMaster> {
     constructor(private http: AppService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<CategoryMaster> | Promise<CategoryMaster> | CategoryMaster {
        return  this.http.get(apiConfig.getAllCategory);
    }

}