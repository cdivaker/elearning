import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';
import { ActivatedRoute } from '@angular/router';
import { UserMenu } from 'src/app/model/menu';
import { CategoryMaster } from '../../../model/category';

@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.scss']
})
export class HomeLayoutComponent implements OnInit {
menuItem:UserMenu;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    

    if(this.route.snapshot.data.menu)
    this.menuItem=this.route.snapshot.data.menu.result;

  }

}
