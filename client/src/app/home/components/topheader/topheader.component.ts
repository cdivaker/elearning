import { Component, OnInit,Input,HostListener } from '@angular/core';
import { MatDialogRef, MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';
import { CommonService } from 'src/app/service/common.service';
import { UserMenu } from 'src/app/model/menu';
import { AuthService } from 'src/app/service/auth-service';
import { AppService } from 'src/app/service/app.service';
import { apiConfig } from 'src/app/service/application-setting';
import { CategoryMaster } from '../../../model/category';
import { Router } from '@angular/router';
@Component({
  selector: 'app-topheader',
  templateUrl: './topheader.component.html',
  styleUrls: ['./topheader.component.scss']
})
export class TopheaderComponent implements OnInit {
  @HostListener('document:click', ['$event'])
    documentClick(event: MouseEvent) {
       if(event.target['hasAttribute']('data-notification')){
      if(this.openNotificationClass=='toggle-message-add')
        {
          this.openNotificationClass='';
          this.openWishListClass='';
          }
        else{ 
          this.openNotificationClass='toggle-message-add';
          this.openWishListClass='';
          }
       }
       else if(event.target['hasAttribute']('data-wishlist')){
          if(this.openWishListClass=='toggle-message-add')
        {
          this.openWishListClass='';
          this.openNotificationClass='';
          }
        else{ 
          this.openWishListClass='toggle-message-add';
          this.openNotificationClass='';
          }
       }
       
       else{
         this.openNotificationClass='';
         this.openWishListClass=''
       }
    }
@Input () menudetail:UserMenu;
openNotificationClass='';
openWishListClass='';
category:CategoryMaster[];
isUserLoggedIn:boolean=false;
  constructor(private appService:AppService,private router:Router, private dialog:MatDialog,private commonService:CommonService,private authService:AuthService) { 
 this.loadMenu();

  }

  ngOnInit(): void {
     this.commonService.getAllMenu().subscribe((menus)=>{
       this.menudetail=menus;
        this.isUserLoggedIn=this.authService.isTokenExpired();
       });
       this.isUserLoggedIn=this.authService.isTokenExpired();
       this.getAllCategory();
  }

  getAllCategory(){
    this.appService.get(apiConfig.getAllCategory).subscribe(arg => {
      
    this.category = arg.result;
    }
    );
  }
search(evt){
if(evt.target.value)
this.router.navigate(['/elearn/course/courselist'],{ queryParams: { reference: evt.target.value } });

}
  login(){
    //throw Error("Validation error without auto or default message set!");
      let editItem=null;
      let message='Client added successfully.';
     
      let dialogRef: MatDialogRef<LoginComponent>;
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = { editItem: editItem };
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = false;
   
      dialogConfig.minWidth="30vw";
      dialogConfig.minHeight="33vw";
       dialogConfig.maxWidth = "75vw";
      
       dialogConfig.maxHeight = "100vw";
     
      dialogRef = this.dialog.open(LoginComponent, dialogConfig);
      dialogRef.disableClose = true;
      dialogRef.afterClosed().subscribe(result => {
        
        });
    
  }
  logout(){
    this.authService.logout();
    this.loadMenu();
    this.router.navigate(['/elearn']);
  }

  loadMenu(){
    
     this.appService.get(apiConfig.getUserMenu).subscribe((response)=>{
       this.commonService.setAllMenu(response.result);
       })
  }

}
