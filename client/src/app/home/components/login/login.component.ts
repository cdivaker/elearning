import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators, PatternValidator } from '@angular/forms';
import { RxFormBuilder, ReactiveFormConfig, RxFormGroup } from '@rxweb/reactive-form-validators';
import { LoginUser } from 'src/app/model/login';
import { AppService } from 'src/app/service/app.service';
import { apiConfig } from 'src/app/service/application-setting';
import { UserDetail } from 'src/app/model/user-details';
import { AuthService } from 'src/app/service/auth-service';
import { CommonService } from '../../../service/common.service';
import { LoaderService } from '../../../shared/common/loader.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers:[RxFormBuilder]
})
export class LoginComponent implements OnInit {
  frmLogin :RxFormGroup;
  frmRegister:RxFormGroup;
  user:LoginUser;
  userDetail:UserDetail;
  isLoading:boolean=false;
  isLoginLoading:boolean=false;
  constructor(private commonService: CommonService,private loader:LoaderService,  private activatedRoute: ActivatedRoute,private authService:AuthService, private  appService:AppService,  private router:Router,private formBuilder: RxFormBuilder,
    public dialogRef: MatDialogRef<LoginComponent>) {
      
      this.user= new LoginUser();
      this.userDetail= new UserDetail();
        this.frmLogin =<RxFormGroup>this.formBuilder.formGroup(this.user);
        this.frmRegister=<RxFormGroup>this.formBuilder.formGroup(this.userDetail);
     }

  ngOnInit(): void {
    ReactiveFormConfig.set({
      "validationMessage":{
        "required":"This field is required"
        },
        "reactiveForm":{
          "errorMessageBindingStrategy": ErrorBindingStrategy.onSubmit |ErrorBindingStrategy.onDirtyOrTouched
        }
      }
        );
     
  }
  login(){
    this.loader.display(true);
      //this.isLoginLoading=true;
      this.authService.login(this.user).subscribe((response)=>{
        //this.isLoginLoading=false;
         setTimeout(() => {
           this.loader.display(false);
            this.appService.get(apiConfig.getUserMenu).subscribe((response)=>{
       this.commonService.setAllMenu(response.result);
       this.dialogRef.close();
      
       })
       }, 3000);
        localStorage.setItem('auth_token', response.access_token);
      
        // this.isLoginLoading=false;
        // console.log(response);
      
          },(errro)=>{
            this.isLoginLoading=false;
            this.loader.display(false);
          })
  }
  closeDialog(): void {
    this.dialogRef.close();
  }
  signUp(){
    this.loader.display(true);
    if(this.frmRegister.valid){
     // this.isLoading=true;
      this.appService.post(apiConfig.register, this.userDetail).subscribe((response)=>{
    
      this.user.username=this.userDetail.email;
      this.user.password=this.userDetail.password;
      //  this.isLoading=false;
        console.log(response);
        this.login();
      },((error)=>{

this.loader.display(false);
this.isLoading=false;
      }))
    }
  }

}

export enum ErrorBindingStrategy{
  none,
  onSubmit,
  onDirty,
  onTouched,
  onDirtyOrTouched
 }
