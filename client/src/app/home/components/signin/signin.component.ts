import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, PatternValidator } from '@angular/forms';
import { RxFormBuilder, ReactiveFormConfig, RxFormGroup } from '@rxweb/reactive-form-validators';
import { LoginUser } from 'src/app/model/login';
import { AppService } from 'src/app/service/app.service';
import { apiConfig } from 'src/app/service/application-setting';
import { UserDetail } from 'src/app/model/user-details';
import { AuthService } from 'src/app/service/auth-service';
import { CommonService } from '../../../service/common.service';
import { LoaderService, MessageBox } from '../../../shared/common/loader.service';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
user:LoginUser;
  isLoading:boolean=false;
  isLoginLoading:boolean=false;
    frmLogin :RxFormGroup;
  constructor(private commonService: CommonService,private loader:LoaderService,  private activatedRoute: ActivatedRoute,private authService:AuthService, private  appService:AppService,  private router:Router,private formBuilder: RxFormBuilder
   ) {
      
      this.user= new LoginUser();
        this.frmLogin=<RxFormGroup>this.formBuilder.formGroup(this.user);
     }

  ngOnInit(): void {
      ReactiveFormConfig.set({
      "validationMessage":{
        "required":"This field is required"
        },
        "reactiveForm":{
          "errorMessageBindingStrategy": ErrorBindingStrategy.onSubmit |ErrorBindingStrategy.onDirtyOrTouched
        }
      }
        );
  }
  login(){
    debugger

    this.loader.display(true);
      this.authService.login(this.user).subscribe((response)=>{
         setTimeout(() => {
           this.loader.display(false);
            this.appService.get(apiConfig.getUserMenu).subscribe((response)=>{
            this.commonService.setAllMenu(response.result);
             this.router.navigate(['/elearn']);
       })
       }, 3000);
        localStorage.setItem('auth_token', response.access_token);
      
          },(error)=>{
            this.isLoginLoading=false;
             new MessageBox().show(error.error.exceptionMessage,'danger');
            this.loader.display(false);
          })
  }


}

export enum ErrorBindingStrategy{
  none,
  onSubmit,
  onDirty,
  onTouched,
  onDirtyOrTouched
 }
