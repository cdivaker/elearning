import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, PatternValidator } from '@angular/forms';
import { RxFormBuilder, ReactiveFormConfig, RxFormGroup } from '@rxweb/reactive-form-validators';
import { LoginUser } from 'src/app/model/login';
import { AppService } from 'src/app/service/app.service';
import { apiConfig } from 'src/app/service/application-setting';
import { UserDetail } from 'src/app/model/user-details';
import { AuthService } from 'src/app/service/auth-service';
import { CommonService } from '../../../service/common.service';
import { LoaderService, MessageBox } from '../../../shared/common/loader.service';
@Component({
  selector: 'app-become-author',
  templateUrl: './become-author.component.html',
  styleUrls: ['./become-author.component.scss']
})
export class BecomeAuthorComponent implements OnInit {
frmRegister:RxFormGroup;
  userDetail:UserDetail;
  constructor(private commonService: CommonService,private loader:LoaderService,  private activatedRoute: ActivatedRoute,private authService:AuthService, private  appService:AppService,  private router:Router,private formBuilder: RxFormBuilder
   ) {
      
        this.userDetail= new UserDetail();
         this.userDetail.usertype='author';
        this.frmRegister=<RxFormGroup>this.formBuilder.formGroup(this.userDetail);
     }

  ngOnInit(): void {
     ReactiveFormConfig.set({
      "validationMessage":{
        "required":"This field is required"
        },
        "reactiveForm":{
          "errorMessageBindingStrategy": ErrorBindingStrategy.onSubmit |ErrorBindingStrategy.onDirtyOrTouched
        }
      }
        );
  }

  becomeAuthor(){
   
    this.loader.display(true);
    if(this.frmRegister.valid){
      this.appService.post(apiConfig.register, this.userDetail).subscribe((response)=>{
         this.loader.display(false);
          this.router.navigate(['/elearn']);
        new MessageBox().show('Thanks for registered with us your username and password has been sent to provided email id','success');
      },((error)=>{
         new MessageBox().show(error.error.exceptionMessage,'danger');
            this.loader.display(false);
      }))
    }
  }

}

export enum ErrorBindingStrategy{
  none,
  onSubmit,
  onDirty,
  onTouched,
  onDirtyOrTouched
 }
