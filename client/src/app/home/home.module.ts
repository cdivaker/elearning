import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { RxFormBuilder, RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppService } from '../service/app.service';
import { HomeLayoutComponent } from './components/home-layout/home-layout.component';
import { LoginComponent } from './components/login/login.component';
import { CategoryComponent } from './components/category/category.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { TestComponent } from './components/test/test.component';
import { MaterialModule } from '../shared/material/material.module';
import { AuthService } from '../service/auth-service';
import { TopheaderComponent } from './components/topheader/topheader.component';
import { SigninComponent } from './components/signin/signin.component';
import { BecomeAuthorComponent } from './components/become-author/become-author.component';
import { SignupComponent } from './components/signup/signup.component';




@NgModule({
  declarations: [HomeLayoutComponent,SigninComponent,BecomeAuthorComponent, HomeLayoutComponent, LoginComponent, CategoryComponent, WelcomeComponent, TestComponent, TopheaderComponent, SignupComponent],
  imports: [    
    HomeRoutingModule,
    MaterialModule,
    CommonModule,
    CarouselModule,
    FormsModule,
    ReactiveFormsModule ,
    HttpClientModule
  ],providers:[AppService ]
})
export class HomeModule { }
