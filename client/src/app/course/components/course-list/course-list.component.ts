import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from '../../../service/app.service';
import { CategoryMaster } from '../../../model/category';
import { apiConfig } from 'src/app/service/application-setting';
import { Subscription, Observable } from 'rxjs';
@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {
categoryList:CategoryMaster[];
category='';
routeSubscription: Subscription;
  constructor(private router:Router,private appService:AppService, private route: ActivatedRoute) { 
    this.getAllCategory();
  }
  array =[1,2,3,4];
  gridViewListViewClass='content grid';
  ngOnInit(): void {


  this.routeSubscription=this.route.queryParams
      .subscribe(params => {
        this.category = params['reference'];
      });
    
  }
  goto(){
    this.router.navigate(['/elearn/course/courseinfo'])
  }
  changeView(view){
    if(view=='list')
    this.gridViewListViewClass='content list fade-2';
    if(view=='grid')
    this.gridViewListViewClass='content grid fade-2';
  }

getAllCategory(){
    this.appService.get(apiConfig.getAllCategory).subscribe(arg => {
    this.categoryList = arg.result;
    }
    );
  }
  filter(category){

  }
ngOnDestroy(){
  this.routeSubscription.unsubscribe();
}
}
