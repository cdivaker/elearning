import { required ,maxLength,prop} from "@rxweb/reactive-form-validators"
export class UserDetail {
     
    @required()
    email :string

     @required({conditionalExpression:'x => x.usertype == "Learner"' })
     password :string  

     @required({conditionalExpression:'x => x.usertype == "Learner"' })  
     firstName :string

     @required()
     userName :string

    @required({conditionalExpression:'x => x.usertype == "Learner"' })
     lastName :string
     address1 :string
     address2 :string
     city :string
     state :string
     postalCode :string

      @required({conditionalExpression:'x => x.usertype == "author"' })
     phonePrimary :string
     phoneSecondary :string  

     @prop()
     
     usertype :string='Learner'
  
}