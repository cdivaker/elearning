import { NgModule } from '@angular/core';
import { Routes, RouterModule, Resolve } from '@angular/router';
import { HomeLayoutComponent } from './home/components/home-layout/home-layout.component';
import { UserMenuResolver } from './home/shared/resolver/usermenu.resolver';
import { CategoryResolver } from './home/shared/resolver/category.resolver';


const routes: Routes = [
  {
    path:'',redirectTo:'elearn',pathMatch:'full'
  },
  {
    path: 'elearn',
    component: HomeLayoutComponent,
    resolve:{menu:UserMenuResolver},
    children: [
      {
      path:'',
      loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
    }
  ]
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
