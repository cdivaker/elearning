import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';
import { apiConfig } from './application-setting';
import { LoginUser } from '../model/login';
@Injectable({
  providedIn: 'root'
  
})
export class AppService {
  baseApiUrl='http://localhost:52200/api/';
  constructor(private http: HttpClient) { 
    this.baseApiUrl='http://localhost:52200/api/'
  }

  get(url, parameter:any={}): Observable<any> {
    return this.http.get(this.baseApiUrl + url, { params: parameter }).pipe(
        map((data) => {
            return data;
        })
    );
}


post(url,body): Observable<any> {
 
   let options: any = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
};
    return this.http.post(this.baseApiUrl + url,JSON.stringify(body),options).pipe(
        map((data) => {
            return data;
        }
    ));
}


put(url,body){

}

delete(url,params){

}
  
}
