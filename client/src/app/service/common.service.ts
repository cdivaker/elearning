import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';
import { UserMenu } from 'src/app/model/menu';
@Injectable({
  providedIn: 'root'
})
export class CommonService {
    private menuSubject = new Subject<UserMenu>();
   
    getAllMenu(): Observable<UserMenu>{
        return this.menuSubject.asObservable();
      }
    
    setAllMenu(menu : UserMenu) {
        this.menuSubject.next(menu);
      }
}


 