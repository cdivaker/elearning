import { Injectable } from '@angular/core';
import decode from 'jwt-decode';
import { LoginUser } from '../model/login';
import { apiConfig } from './application-setting';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as jwt_decode from 'jwt-decode';
import { map } from 'rxjs/operators';
@Injectable()
export class AuthService {
    
  
 

  private  baseApiUrl='http://localhost:52200/api/';
  private headers = new Headers({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) { }

  public getToken(): string {
    return localStorage.getItem('auth_token');
  }
  
  setToken(token: string): void {
    localStorage.setItem('auth_token', token);
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);
    if (decoded.exp === undefined) return null;
    const date = new Date(0); 
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if(!token) token = this.getToken();
    if(token) return true;
    else return false;

    // const date = this.getTokenExpirationDate(token);
    // if(date === undefined) return false;
    // return !(date.valueOf() > new Date().valueOf());
  }


  login(user: LoginUser) {
    var userData = 'username=' + user.username + '&password=' + user.password + '&grant_type=password';
     var reqHeader = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'No-Auth': 'True',
    });
    return this.http
      .post<any>(this.baseApiUrl + apiConfig.login, encodeURI(userData), { headers: reqHeader })
      .pipe(
        map((data) => {
          return data;
        })
      );
  }
  logout(){
    localStorage.removeItem('auth_token');
  }


}