import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
declare var $: any;
@Injectable({
  providedIn: 'root'
})
@Injectable()
export class LoaderService {

constructor(private router: Router) {
 
  this.router.events.subscribe(event => {
    
    if(event instanceof NavigationStart) {
       //this.status.next(true);
      console.log('NavigationStart')
     // this.display(true);
    }
    if(event instanceof NavigationEnd) {
      //this.display(false);
      console.log('NavigationEnd');
      window.scrollTo({
  top: 0,
  behavior: 'smooth',
})
      //this.status.next(false);
    }
  });
}

  public status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  display(value: boolean) {
    setTimeout(() => {
      this.status.next(value);
    }, 100);
      
  }

 

}


export class MessageBox {
 
  // hide(){
  //   this.display(false);
  // }
   show(message,type){
     $.notifyClose();
        //const type = ['','info','success','warning','danger'];
        let msg='Success : '
    if(type=='danger'){
      msg='Error : '
    }
        const color = Math.floor((Math.random() * 4) + 1);
        $.notify({
          title:msg,
            icon: 'notifications',
            message: message,
        },{
            // settings
           
            position: 'fixed',
            type:type,
            allow_dismiss: true,
            newest_on_top: true,
            showProgressbar: false,
            
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 5000,
            timer: 600,
            url_target: '_blank',
            mouse_over: null,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            onShow: null,
            onShown: null,
            onClose: function(e) {
              
             
              },
            onClosed: function(e) {
              
              },
                onClick: function(e) {
              
              },
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
            '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
            '<i class="material-icons" data-notify="icon">notifications</i> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
              '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
          '</div>' 
        });
    }

    error(message){
$.notify({
    title: 'Error',
    icon: "fa fa-exclamation-triangle",
     message: message,
}, {
   offset: 20,
            spacing: 10,
  type:'danger',
    timer: 600,
    autoHide: false,
    clickToHide: false,
});
    }
}
