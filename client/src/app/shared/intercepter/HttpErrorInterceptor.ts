import { HttpEvent, 
    HttpInterceptor, 
    HttpHandler, 
    HttpRequest, 
    HttpResponse,
    HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MessageBox } from '../common/loader.service';

export class HttpErrorInterceptor implements HttpInterceptor {
intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
return next.handle(request)
  .pipe(
    catchError( (error: HttpErrorResponse) => { 
      
       let errMsg = '';

       if(error.status!=200 && error.status!=401){
         new MessageBox().show(error.error.exceptionMessage,'danger')
       }else if(error.status==401){
        new MessageBox().error(error.error.message)
       }
       // Client Side Error
       if (error.error instanceof ErrorEvent) {   
         errMsg = `Error: ${error.error.message}`;
       } 
       else {  // Server Side Error
         errMsg = `Error Code: ${error.status},  Message: ${error.message}`;
       }
       return throwError(errMsg);
     })
  )
}


}   