import { Component } from '@angular/core';
import { LoaderService } from './shared/common/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';
 showLoader :boolean= false;
  connection:any;
  proxy:any;
  constructor(private loaderService:LoaderService) {
  }
  ngOnInit(){
    setTimeout(() => {
      this.showLoader = false;
    this
    .loaderService
    .status
    .subscribe((val : boolean) => {
      this.showLoader = val;
    });
    }, 500);
    
  }
}
