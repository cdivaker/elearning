﻿using PMS.Framework;
using PMS.IProvider;
using PMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PMS.WebApi.Controllers
{
    [RoutePrefix("api/category")]
    [AllowAnonymous]
    public class CategoryController : ApiController
    {
        private readonly ICategoryProvider _categoryProvider;
        protected readonly ILoggerManager _ILogger;
        public CategoryController(ICategoryProvider categoryProvider, ILoggerManager Logger) : base()
        {
            _ILogger = Logger;
            _categoryProvider = categoryProvider;
        }
        [Route("add")]
        [HttpPost]
        public IHttpActionResult SaveCategory([FromBody] CategoryDTO courseDetailDTO)
        {
            try
            {
                var result = _categoryProvider.Add(courseDetailDTO);
                if (result == null)
                {
                    throw new Exception(string.Join(",", "CourseDetail not saved"));
                }
                else
                {
                    var response = new Response()
                    {
                        Code = (int)HttpStatusCode.Created,
                        Status = HttpStatusCode.Created.ToString(),
                        Result = result
                    };
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                _ILogger.Error(ex.Message, ex);
                throw ex;
            }
        }

        [Route("getAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                var result = _categoryProvider.GetAll();
                if (result.Count() == 0)
                {
                    throw new Exception(string.Join(",", "Category Detail not exists"));
                }
                else
                {
                    var response = new Response()
                    {
                        Code = (int)HttpStatusCode.Created,
                        Status = HttpStatusCode.Created.ToString(),
                        Result = result
                    };
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                _ILogger.Error(ex.Message, ex);
                throw ex;
            }
        }

        [Route("get")]
        [HttpGet]
        public IHttpActionResult GetCourseById([FromUri]int id)
        {
            try
            {
                var result = _categoryProvider.Get(id);
                if (result == null)
                {
                    throw new Exception(string.Join(",", "CourseDetail not saved"));
                }
                else
                {
                    var response = new Response()
                    {
                        Code = (int)HttpStatusCode.Created,
                        Status = HttpStatusCode.Created.ToString(),
                        Result = result
                    };
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                _ILogger.Error(ex.Message, ex);
                throw ex;
            }
        }

    }
}
