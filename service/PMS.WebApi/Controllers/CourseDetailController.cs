﻿using PMS.Framework;
using PMS.IProvider;
using PMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PMS.WebApi.Controllers
{
    [RoutePrefix("api/course")]
    public class CourseDetailController : ApiController
    {
        private readonly ICourseDetailProvider _courseDetailProvider;
        protected readonly ILoggerManager _ILogger;
        public CourseDetailController(ICourseDetailProvider courseDetailProvider, ILoggerManager Logger) : base()
        {
            _ILogger = Logger;
            _courseDetailProvider = courseDetailProvider;
        }
        [Route("add")]
        [HttpPost]
        public IHttpActionResult SaveCourseDetail([FromBody] CourseDetailDTO courseDetailDTO)
        {
            try
            {
                var result = _courseDetailProvider.Add(courseDetailDTO);
                if (result == null)
                {
                    throw new Exception(string.Join(",", "CourseDetail not saved"));
                }
                else
                {
                    var response = new Response()
                    {
                        Code = (int)HttpStatusCode.Created,
                        Status = HttpStatusCode.Created.ToString(),
                        Result = result
                    };
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                _ILogger.Error(ex.Message, ex);
                throw ex;
            }
        }

        [Route("getAll")]
        [HttpPost]
        public IHttpActionResult GetAll()
        {
            try
            {
                var result = _courseDetailProvider.GetAll();
                if (result.Count() == 0)
                {
                    throw new Exception(string.Join(",", "CourseDetail not saved"));
                }
                else
                {
                    var response = new Response()
                    {
                        Code = (int)HttpStatusCode.Created,
                        Status = HttpStatusCode.Created.ToString(),
                        Result = result
                    };
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                _ILogger.Error(ex.Message, ex);
                throw ex;
            }
        }

        [Route("get")]
        [HttpPost]
        public IHttpActionResult GetCourseById([FromUri]int id)
        {
            try
            {
                var result = _courseDetailProvider.Get(id);
                if (result == null)
                {
                    throw new Exception(string.Join(",", "CourseDetail not saved"));
                }
                else
                {
                    var response = new Response()
                    {
                        Code = (int)HttpStatusCode.Created,
                        Status = HttpStatusCode.Created.ToString(),
                        Result = result
                    };
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                _ILogger.Error(ex.Message, ex);
                throw ex;
            }
        }

    }
}
