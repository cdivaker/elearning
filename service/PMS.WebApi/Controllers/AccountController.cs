﻿using PMS.Framework;
using PMS.IProvider;
using PMS.Models;
using System.Collections.Generic;
using System.Web.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Net;
using System.Text;
using System;

namespace PMS.WebApi.Controllers
{
    [RoutePrefix("api/user")]
    [Authorize]
    public class AccountController : BaseController
    {
        private readonly IAccountProvider _userProvider;
        protected readonly ILoggerManager _ILogger;
        public AccountController(IAccountProvider userProvider, ILoggerManager Logger) : base()
        {
            _ILogger = Logger;
            _userProvider = userProvider;
        }

        [Route("get")]
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult Test()
        {
            return Ok(true);
        }


        #region User Manage Public Methods

        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
                if (i == 3)
                {
                    builder.Append("@");
                }
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Register([FromBody]UserDTO userDTO)
        {
            try
            {
                //if request for become tutor then mail will sent and password auto genrrtae
                if (userDTO.UserType.ToLower() == "author")
                {
                    new Random().Next(5, 100);
                    StringBuilder builder = new StringBuilder();
                    builder.Append(RandomString(4, true));
                    builder.Append(new Random().Next(1000, 9999));
                    builder.Append(RandomString(2, false));
                    userDTO.Password = builder.ToString();
                }

                var data = await _userProvider.RegisterUser(userDTO, userDTO.Password);
                if (data.Count() > 0)
                {
                    throw new System.Exception(string.Join(",", data));
                }
                else
                {

                    var response = new Response()
                    {
                        Code = (int)HttpStatusCode.Created,
                        Status = HttpStatusCode.Created.ToString(),
                        Result = data
                    };

                    return Ok(response);
                }
            }
            catch (System.Exception ex)
            {
                _ILogger.Error(ex.Message, ex);
                throw ex;
            }

        }


        [HttpPost]
        [Route("update")]
        [Authorize(Roles = "Author,Learner")]
        public IHttpActionResult UpdateUserPrfile([FromBody]UserDTO userDTO)
        {
            try
            {
                //var name = User.Identity.cl;
                var data = _userProvider.UpdateUser(userDTO);
                var response = new Response()
                {
                    Code = (int)(data.Count() > 0 ? HttpStatusCode.BadRequest : HttpStatusCode.Created),
                    Status = (data.Count() > 0 ? HttpStatusCode.BadRequest : HttpStatusCode.Created).ToString(),
                    Result = data
                };
                return Ok(response);
            }
            catch (System.Exception ex)
            {
                _ILogger.Error(ex.Message, ex);
                throw ex;
            }

        }


        [HttpGet]
        [Route("getUserMenu")]
        [AllowAnonymous]
        public IHttpActionResult GetUserMenu()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var data = _userProvider.GetUserMenu();
                    var response = new Response()
                    {
                        Code = (int)HttpStatusCode.OK,
                        Status = HttpStatusCode.OK.ToString(),
                        Result = data
                    };
                    return Ok(response);
                }
                else
                {
                    return Ok(new Response());
                }
               
            }
            catch (System.Exception ex)
            {
                _ILogger.Error(ex.Message);
                throw ex;
            }

        }
        #endregion

    }
}
