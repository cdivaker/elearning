﻿using PMS.IProvider;
using PMS.IRepository;
using PMS.Models;
using PMS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Provider
{
   public class CourseDetailProvider: ICourseDetailProvider
    {
        private readonly IPMSRepository<CourseDetailDTO> _commanRepository;

        public CourseDetailProvider(IPMSRepository<CourseDetailDTO> courseDetailRepository)
        {
            _commanRepository = courseDetailRepository;
        }
        public CourseDetailDTO Add(CourseDetailDTO courseDetailDTO)
        {
            return _commanRepository.Add(courseDetailDTO);
        }

        public CourseDetailDTO Get(int id)
        {
            return _commanRepository.Get(id);

        }

        public IQueryable<CourseDetailDTO> GetAll()
        {
            return _commanRepository.GetAll();
        }
    }
}
