﻿using ExpressMapper;
using ExpressMapper.Extensions;
using PMS.IProvider;
using PMS.IRepository;
using PMS.Models;
using PMS.Repository.DBEntity.MasterEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Provider
{
   public class CategoryProvider: ICategoryProvider
    {
        private readonly IPMSRepository<CategoryMaster> _commanRepository;

        public CategoryProvider(IPMSRepository<CategoryMaster> commanRepository)
        {
            _commanRepository = commanRepository;
        }

        public CategoryDTO Get(int id)
        {
           var categoryMaster= _commanRepository.Get(id);
            return Mapper.Map<CategoryMaster, CategoryDTO>(categoryMaster); 
        }

        public IQueryable<CategoryDTO> GetAll()
        {
            var categoryMaster= _commanRepository.GetAll();
            return Mapper.Map<IQueryable<CategoryMaster>, IQueryable<CategoryDTO>>(categoryMaster);
        }

        public CategoryDTO Add(CategoryDTO categoryDTO)
        {
           var categoryMaster= Mapper.Map<CategoryDTO, CategoryMaster> (categoryDTO);
             _commanRepository.Add(categoryMaster);
            return categoryDTO;
        }
    }
}
