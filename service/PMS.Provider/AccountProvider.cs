﻿
using ExpressMapper;
using ExpressMapper.Extensions;
using PMS.IProvider;
using PMS.IRepository;
using PMS.Models;
using PMS.Repository;
using PMS.Repository.DBEntity.MasterEntity;
using PMS.Repository.DBEntity.UsersEntity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace PMS.Provider
{
    public class AccountProvider : IAccountProvider
    {
       // private readonly IMapper _mapper;
        private readonly IAccountRepository _userRepository;
        private readonly IPMSRepository<MenuRoleMapping> _commanRepository;
        private readonly IPMSRepository<RoleMaster> _role;

        public AccountProvider(IAccountRepository userRepository, IPMSRepository<MenuRoleMapping> pMSRepository, IPMSRepository<RoleMaster> role)
        {
            //, IMapper mapper
            // _mapper = mapper;
            _role = role;
            _userRepository = userRepository;
            _commanRepository = pMSRepository;
        }
        public Task<IEnumerable<string>> RegisterUser(UserDTO user, string password)
        {
            
            return _userRepository.RegisterUser(user, password);
        }

        public Task<UserDTO> FindAsync(string userName, string password)
        {
            return _userRepository.FindAsync(userName, password);
        }

        public IEnumerable<string> UpdateUser(UserDTO userDTO)
        {
            return _userRepository.UpdateUser(userDTO);
        }

        public List<UserMenuDTO> GetUserMenu()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

            // Get the claims values
            var role = identity.Claims.Where(c => c.Type == "Role").FirstOrDefault().Value;
            var userid = identity.Claims.Where(c => c.Type == "UserId").FirstOrDefault().Value;


            string roleId = _role.Get(x=>x.Name.ToLower() == role.ToLower()).FirstOrDefault().Id;
            var menuMasters = _commanRepository.Get(x=>x.RoleId== roleId).Include(x=>x.MenuMaster).Select(x=>x.MenuMaster).ToList();
            var  response=  Mapper.Map<List<MenuMaster> ,List<UserMenuDTO>>(menuMasters);
            return response;
        }

       
    }
}
