﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Models
{
  public class UserMenuDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Uri { get; set; }
    }
}
