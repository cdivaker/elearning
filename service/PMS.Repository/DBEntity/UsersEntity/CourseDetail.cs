﻿using PMS.Repository.DBEntity.MasterEntity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS.Repository.DBEntity.UsersEntity
{
    public class CourseDetail:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("CategoryMaster")]
        public int CategoryId { get; set; }
        public CategoryMaster CategoryMaster { get; set; }

        public string CourseType { get; set; }
        public string CourseName { get; set; }
        public string Description { get; set; }
        public DateTime OpenDate { get; set; }
        public int Tuitionfee { get; set; }
        public int Duration { get; set; }
        public int CourseLevel { get; set; }
        public bool IsCoursePublished { get; set; }
        public int UserId { get; set; }

    }
}
