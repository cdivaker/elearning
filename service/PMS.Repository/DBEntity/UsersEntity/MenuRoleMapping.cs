﻿using PMS.Repository.DBEntity.MasterEntity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS.Repository.DBEntity.UsersEntity
{
    public class MenuRoleMapping
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }


        [ForeignKey("RoleMaster")]
        public string RoleId { get; set; }
        public RoleMaster RoleMaster { get; set; }


        [ForeignKey("MenuMaster")]
        public int MenuId { get; set; }
        public MenuMaster MenuMaster { get; set; }
    }
}
