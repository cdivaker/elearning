namespace PMS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class coursedetailcreated : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategoryMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CourseDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        CourseType = c.String(),
                        CourseName = c.String(),
                        Description = c.String(),
                        OpenDate = c.DateTime(nullable: false),
                        Tuitionfee = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        CourseLevel = c.Int(nullable: false),
                        IsCoursePublished = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryMasters", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CourseDetails", "CategoryId", "dbo.CategoryMasters");
            DropIndex("dbo.CourseDetails", new[] { "CategoryId" });
            DropTable("dbo.CourseDetails");
            DropTable("dbo.CategoryMasters");
        }
    }
}
