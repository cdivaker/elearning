// <auto-generated />
namespace PMS.Repository.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class coursedetailcreated : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(coursedetailcreated));
        
        string IMigrationMetadata.Id
        {
            get { return "202005240804381_coursedetailcreated"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
