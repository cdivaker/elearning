namespace PMS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class menutablecreated : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MenuMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Uri = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MenuRoleMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.String(maxLength: 128),
                        MenuId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuMasters", t => t.MenuId, cascadeDelete: true)
                .ForeignKey("dbo.RoleMaster", t => t.RoleId)
                .Index(t => t.RoleId)
                .Index(t => t.MenuId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MenuRoleMappings", "RoleId", "dbo.RoleMaster");
            DropForeignKey("dbo.MenuRoleMappings", "MenuId", "dbo.MenuMasters");
            DropIndex("dbo.MenuRoleMappings", new[] { "MenuId" });
            DropIndex("dbo.MenuRoleMappings", new[] { "RoleId" });
            DropTable("dbo.MenuRoleMappings");
            DropTable("dbo.MenuMasters");
        }
    }
}
