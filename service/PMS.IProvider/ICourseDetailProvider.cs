﻿using PMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.IProvider
{
    public interface ICourseDetailProvider
    {
        CourseDetailDTO Add(CourseDetailDTO courseDetailDTO);
        IQueryable<CourseDetailDTO> GetAll();
        CourseDetailDTO Get(int id);
    }
}
