﻿using PMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.IProvider
{
   public interface ICategoryProvider
    {
        CategoryDTO Add(CategoryDTO categoryDTO);
        IQueryable<CategoryDTO> GetAll();
        CategoryDTO Get(int id);
    }
}
